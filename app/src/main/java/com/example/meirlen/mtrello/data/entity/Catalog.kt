package com.example.meirlen.mtrello.data.entity


data class Catalog(val city_id: Int,
                   val id: Int,
                   val name: String,
                   val slug: String,
                   val icon: String,
                   val products: List<Product>

){

}

data class Product(
        val id: Int,
        val category_id: Int,
        val name: String,
        val description: String,
        val price: Int
)