package com.example.meirlen.mtrello.domain.repository

import com.example.meirlen.mtrello.data.entity.Catalog
import io.reactivex.Single

interface CatalogRepository {

    fun getCatalogTree(): Single<List<Catalog>>

}