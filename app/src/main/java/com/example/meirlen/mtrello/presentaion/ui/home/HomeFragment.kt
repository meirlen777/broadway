package com.example.meirlen.mtrello.presentaion.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.data.entity.Catalog
import com.example.meirlen.mtrello.data.entity.Product
import com.example.meirlen.mtrello.presentaion.base.vo.Status.*
import com.example.meirlen.mtrello.presentaion.base.custom.HidingScrollListener
import com.example.meirlen.mtrello.presentaion.base.custom.OffsetXItemDecoration
import com.example.meirlen.mtrello.presentaion.ui.home.adapter.CatalogAdapter
import com.example.meirlen.mtrello.presentaion.ui.home.adapter.ProductAdapter
import com.example.meirlen.mtrello.presentaion.utill.DataParser
import com.example.meirlen.mtrello.presentaion.ext.*
import com.example.meirlen.mtrello.presentaion.utill.interfaces.ItemClickListener
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.getViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class HomeFragment : Fragment() {

    private lateinit var catalogAdapter: CatalogAdapter
    private lateinit var productAdapter: ProductAdapter
    private val homeViewModel: HomeViewModel by sharedViewModel()



    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeLiveData()
    }

    private fun observeLiveData() {
        homeViewModel.catalogLiveData.observe(this, Observer {
            when (it?.status) {
                LOADING -> displayProgress()
                SUCCESS -> showData(it.data as ArrayList<Catalog>)
                ERROR -> Toast.makeText(context, "Error: ${it.message}", Toast.LENGTH_SHORT).show()
            }
        })

        homeViewModel.eventLiveData.observe(this, Observer {
            homeViewModel.getCatalog()
        })
    }

    private fun showData(catalogList: ArrayList<Catalog>) {
        catalogAdapter.setData(catalogList) // show categories
        productAdapter.setData(DataParser.getProductsFromCatalog(catalogList)) // show products
        productRv.scrollToPosition(0)
        catalogRv.scrollToPosition(0)
        displayNormal()
    }

    private fun initView() {
        setUpCatalogRecyclerView()
        setUpProductRecyclerView()
        homeViewModel.getCatalog()
    }

    /**
     * Setup recyclerview for products list
     * Vertical list
     */
    private fun setUpProductRecyclerView() {
        productAdapter = ProductAdapter()
        val mLayoutManager = LinearLayoutManager(context)
        productRv.layoutManager = mLayoutManager
        productRv.adapter = productAdapter
        addScrollListenerToProductRecyclerView()

    }

    /**
     * Setup recyclerview for catalog list
     *  Horizontal menu
     */
    private fun setUpCatalogRecyclerView() {
        catalogAdapter = CatalogAdapter(object : ItemClickListener<Catalog> {
            override fun onItemClick(dataObject: Catalog) {
                val position = catalogAdapter.getSelectedItemPosition()
                catalogAdapter.setSelectedItemPosition(position)
                catalogAdapter.notifyDataSetChanged()
                scrollProducts(dataObject.id)
            }
        })

        val mLayoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        catalogRv.layoutManager = mLayoutManager
        catalogRv.adapter = catalogAdapter
        catalogRv.addItemDecoration(OffsetXItemDecoration(context!!))
    }


    /**
     *  Add a listener that will be notified of any changes in product list.
     *  For example:  scroll down/up or if the user clicked category menu
     *  After that there will be changes in the ui:
     *  Scroll Up : hide Toolbar
     *  Scroll Down: show Toolbar
     */

    private fun addScrollListenerToProductRecyclerView() {
        productRv.addOnScrollListener(object : HidingScrollListener() {
            override fun onScrollUpdated() {
                scrollCategories(productAdapter.getProductByPosition(productRv.getCurrentVisibleItem()))
            }

            override fun onHide() {
                mToolBar.hideWithAnimation(mToolBar, catalogRv, productRv)
            }

            override fun onShow() {
                mToolBar.showWithAnimation(mToolBar, catalogRv, productRv)
            }
        })
    }

    /**
     * Starts a smooth scroll for products
     * @param catalogId Using this id we'll find  first visible product position to display
     * @see DataParser.getSelectedProductPosition(catalogId)
     */

    private fun scrollProducts(catalogId: Int) {
        val visiblePosition = DataParser.getSelectedProductPosition(catalogId)
        if (visiblePosition != -1)
            productRv.smoothSnapToPosition(visiblePosition)
    }


    /**
     * Starts a smooth scroll for categories
     * @param product we determine which category the products belong to and and scroll to this category
     * @see DataParser.getCategoryPosition(product)
     */
    private fun scrollCategories(product: Product) {
        val visiblePosition = DataParser.getCategoryPosition(product)
        catalogRv.scrollToPosition(visiblePosition)
        catalogAdapter.setSelectedItemPosition(visiblePosition)
        catalogAdapter.notifyDataSetChanged()
    }

    private fun displayNormal() {
        boardProgressBar.hide()
    }

    private fun displayProgress() {
        boardProgressBar.show()
    }

}