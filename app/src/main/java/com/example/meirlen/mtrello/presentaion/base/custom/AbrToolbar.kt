package com.example.meirlen.mtrello.presentaion.base.custom

import android.content.Context
import androidx.appcompat.widget.Toolbar
import android.util.AttributeSet
import android.view.View
import com.example.meirlen.mtrello.R
import kotlinx.android.synthetic.main.view_abr_toolbar.view.*

class AbrToolbar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : Toolbar(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.view_abr_toolbar, this)
    }

    fun setToolbarPram(title: String) {
        toolbarTitle.text = title
    }
}