package com.example.meirlen.mtrello.presentaion.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import com.example.meirlen.mtrello.presentaion.routers.MainRouter
import org.koin.android.ext.android.inject


class SplashActivity : AppCompatActivity() {

    companion object {
        fun getStartIntent(context: Context): Intent {
            val intent = Intent(context, SplashActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            return intent
        }
    }

    private lateinit var countDownTimer: CountDownTimer
    private val router by inject<MainRouter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        countDownTimer = object : CountDownTimer(500, 500) {
            override fun onFinish() {
                router.opeHomeActivity(this@SplashActivity)
            }

            override fun onTick(millisUntilFinished: Long) {
            }
        }
        countDownTimer.start()
    }

}
