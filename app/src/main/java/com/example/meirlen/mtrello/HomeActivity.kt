package com.example.meirlen.mtrello

import android.content.Context
import android.content.Intent
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayout
import com.example.meirlen.mtrello.presentaion.ext.replaceByTag
import com.example.meirlen.mtrello.presentaion.ui.home.HomeFragment
import com.example.meirlen.mtrello.presentaion.base.custom.SimpleOnTabSelectedListener
import com.example.meirlen.mtrello.presentaion.ext.toast
import com.example.meirlen.mtrello.presentaion.ui.home.HomeViewModel
import com.example.meirlen.mtrello.presentaion.ui.profile.ProfileFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    companion object {

        private const val CURRENT_SCREEN = "current_screen"
        const val HOME = 0
        const val SEARCH = 1
        const val SHARE = 2
        const val FAVOURITE = 3
        const val PROFILE = 4

        fun getStartIntent(context: Context, isNewTask: Boolean = false): Intent {
            val intent = Intent(context, HomeActivity::class.java)
            if (isNewTask) {
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
            }
            return intent
        }
    }

    private var currentScreen: Int = HOME
    private val homeViewModel: HomeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currentScreen = savedInstanceState?.getInt(CURRENT_SCREEN, HOME) ?: HOME
        setupNavigation()
        switchFragment(currentScreen)
    }

    private fun setupNavigation() {
        bottomTabNavigation.getTabAt(currentScreen)?.select()
        bottomTabNavigation.addOnTabSelectedListener(object : SimpleOnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                switchFragment(tab.position)
            }

            override fun onTabReselected(tab: TabLayout.Tab) {
                super.onTabReselected(tab)
                // When the user reselect burger icon we need to refresh catalog
                // We need to notify HomeFragment for this
                if (currentScreen == HOME) {
                    homeViewModel.refreshScreen()
                }
            }
        })
    }

    private fun switchFragment(position: Int) {
        currentScreen = position
        supportFragmentManager.replaceByTag(R.id.frame_container, position.toString(), {
            when (position) {
                HOME -> HomeFragment()
                SEARCH -> ProfileFragment()
                SHARE -> ProfileFragment()
                FAVOURITE -> ProfileFragment()
                PROFILE -> ProfileFragment()
                else -> ProfileFragment()
            }
        }).commit()

    }


}
