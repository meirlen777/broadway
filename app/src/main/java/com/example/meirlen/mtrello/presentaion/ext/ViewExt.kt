package com.example.meirlen.mtrello.presentaion.ext

import android.app.Activity
import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import android.view.animation.AccelerateInterpolator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import com.example.meirlen.mtrello.presentaion.base.custom.AbrToolbar


fun View.setVisibility(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    if (visibility == View.VISIBLE)
        visibility = View.GONE
}

fun View.getDeviceWidth(context: Context): Int {
    val displayMetrics = DisplayMetrics()
    (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.widthPixels
}

fun RecyclerView.smoothSnapToPosition(position: Int, snapMode: Int = LinearSmoothScroller.SNAP_TO_START) {
    val smoothScroller = object : LinearSmoothScroller(this.context) {
        override fun getVerticalSnapPreference(): Int = snapMode
        override fun getHorizontalSnapPreference(): Int = snapMode
    }
    smoothScroller.targetPosition = position
    layoutManager?.startSmoothScroll(smoothScroller)
}


const val duration = 500L
fun AbrToolbar.hideWithAnimation(vararg views: View) {
    val translationYValue = height.toFloat()
    views.forEach {
        it.animate().setDuration(duration).translationY(-translationYValue).interpolator = AccelerateInterpolator(2f)
    }
}

fun AbrToolbar.showWithAnimation(vararg views: View) {
    val translationYValue = 0f
    views.forEach {
        it.animate().setDuration(duration).translationY(translationYValue).interpolator = AccelerateInterpolator(2f)
    }
}

fun RecyclerView.getCurrentVisibleItem() : Int {
    return (layoutManager as LinearLayoutManager)
            .findFirstVisibleItemPosition()
}
