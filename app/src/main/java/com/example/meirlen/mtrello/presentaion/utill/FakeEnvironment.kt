package com.example.meirlen.mtrello.presentaion.utill

import android.content.Context
import com.example.meirlen.mtrello.data.entity.Catalog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.io.InputStream

class FakeEnvironment(private val context: Context) {


    fun getCatalog(): ArrayList<Catalog> {
        val gson = Gson()
        val listType = object : TypeToken<ArrayList<Catalog>>() {}.type
        return gson.fromJson(getStringFromAssets(context, "catalog_list.json"), listType)
    }


    private fun getStringFromAssets(context: Context, fileName: String): String? {
        var str_data: String? = null
        var `is`: InputStream? = null
        try {
            `is` = context.assets.open(fileName)
            val size = `is`!!.available()
            val buffer = ByteArray(size) //declare the size of the byte array with size of the file
            `is`.read(buffer) //read file
            `is`.close() //close file

            // Store text file data in the string variable
            str_data = String(buffer)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return str_data

    }

}