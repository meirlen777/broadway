package com.example.meirlen.mtrello.presentaion.utill

import com.example.meirlen.mtrello.data.entity.Catalog
import com.example.meirlen.mtrello.data.entity.Product


object DataParser {

    private var categoryList: MutableList<Catalog> = mutableListOf()
    private var productList: MutableList<Product> = mutableListOf()


    fun getProductsFromCatalog(catalogList: List<Catalog>): ArrayList<Product> {
        productList.clear()
        categoryList.clear()
        categoryList.addAll(catalogList)

        catalogList.forEach {
            if (it.products.isNotEmpty())
                productList.addAll(it.products)
        }
        return productList as ArrayList<Product>
    }


    fun getSelectedProductPosition(catalogId: Int): Int {
        productList.forEachIndexed { index, product ->
            if (product.category_id == catalogId) {
                var visiblePosition = index
                if (visiblePosition != 0) {
                    visiblePosition += 1
                }
                return index
            }
        }
        return -1
    }


    var categoryPosition = 0
    fun getCategoryPosition(product: Product): Int {
        categoryList.forEachIndexed { index, category ->
            if (product.category_id == category.id) {
                if (categoryPosition!=index){
                    categoryPosition = index
                }
            }
        }
        return categoryPosition
    }

}