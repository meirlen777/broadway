package com.example.meirlen.mtrello.presentaion.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.example.meirlen.mtrello.data.entity.Catalog
import com.example.meirlen.mtrello.presentaion.base.vo.Resource
import com.example.meirlen.mtrello.domain.interactor.GetCatalogUseCase
import com.example.meirlen.mtrello.testing.OpenForTesting

@OpenForTesting
open class HomeViewModel(private val getCatalogUseCase: GetCatalogUseCase) : ViewModel() {


    val catalogLiveData = MutableLiveData<Resource<List<Catalog>>>()
    val eventLiveData = MutableLiveData<Unit>()

    fun getCatalog(): MutableLiveData<Resource<List<Catalog>>> {
        getCatalogUseCase.execute(
                 {
                     catalogLiveData.value = Resource.success(it)
                 },
                 {
                     catalogLiveData.value = Resource.error(it.message.toString(), null)

                 }
         )
        return catalogLiveData
    }

    fun refreshScreen() {
        eventLiveData.value = Unit
    }


}

