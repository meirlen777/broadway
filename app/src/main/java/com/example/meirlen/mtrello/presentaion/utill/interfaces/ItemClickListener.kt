package com.example.meirlen.mtrello.presentaion.utill.interfaces

interface ItemClickListener<in T> {
    fun onItemClick(dataObject : T)
}