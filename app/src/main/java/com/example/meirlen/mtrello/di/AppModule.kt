package com.example.meirlen.mtrello.di
import com.example.meirlen.mtrello.data.impl.CatalogRepositoryImpl
import com.example.meirlen.mtrello.domain.repository.CatalogRepository
import com.example.meirlen.mtrello.domain.interactor.GetCatalogUseCase
import com.example.meirlen.mtrello.presentaion.ui.home.HomeViewModel
import com.example.meirlen.mtrello.presentaion.routers.MainRouter
import com.example.meirlen.mtrello.presentaion.utill.FakeEnvironment
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val archModule = module {
    module("repository") {

        factory {
            CatalogRepositoryImpl(get()) as CatalogRepository
        }
        factory {
            GetCatalogUseCase(get())
        }
        module("viewModel") {
            viewModel {
                HomeViewModel(get())
            }
        }
    }

}
val utilModule = module {
    single {
        MainRouter()
    }
    single {
        FakeEnvironment(get())
    }
}
