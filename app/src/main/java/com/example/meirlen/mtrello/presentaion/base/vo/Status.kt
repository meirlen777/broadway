package com.example.meirlen.mtrello.presentaion.base.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}