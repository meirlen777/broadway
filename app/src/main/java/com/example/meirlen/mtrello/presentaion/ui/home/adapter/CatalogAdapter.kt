package com.example.meirlen.mtrello.presentaion.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.data.entity.Catalog
import com.example.meirlen.mtrello.presentaion.ext.*
import com.example.meirlen.mtrello.presentaion.ext.changeColorFilter
import kotlinx.android.synthetic.main.item_catalog.view.*
import com.example.meirlen.mtrello.presentaion.utill.interfaces.ItemClickListener

class CatalogAdapter(private var listener: ItemClickListener<Catalog>) :
        RecyclerView.Adapter<CatalogAdapter.CatalogViewHolder>() {

    private var catalogList: ArrayList<Catalog> = ArrayList()
    private var selectedItemPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatalogViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_catalog, parent, false)
        return CatalogViewHolder(view)
    }

    override fun onBindViewHolder(holder: CatalogViewHolder, position: Int) {
        val catalog = catalogList[position]
        holder.itemView.catalogIv.loadImage(catalog.icon, 45, 45)

        if (position == selectedItemPosition) {
            holder.itemView.catalogIv.changeColorFilter(R.color.white);
            holder.itemView.circleIvActive.show()
        } else {
            holder.itemView.catalogIv.changeColorFilter(R.color.colorBrown);
            holder.itemView.circleIvActive.hide()
        }

        holder.itemView.setOnClickListener {
            selectedItemPosition = position
            listener.onItemClick(catalog)
        }
    }

    override fun getItemCount(): Int {
        return catalogList.size
    }

    fun setSelectedItemPosition(position: Int) {
        selectedItemPosition = position
    }

    fun getSelectedItemPosition() = selectedItemPosition

    fun setData(movieList: ArrayList<Catalog>) {
        catalogList = movieList
        notifyDataSetChanged()
    }

    fun clearAdapter() {
        catalogList.clear()
        notifyDataSetChanged()
    }

    inner class CatalogViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val view = this.itemView
    }

}