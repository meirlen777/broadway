package com.example.meirlen.mtrello.presentaion.routers

import android.app.Activity
import com.example.meirlen.mtrello.HomeActivity


open  class MainRouter {

    fun opeHomeActivity(context: Activity?) {
        context?.let {
            it.startActivity(HomeActivity.getStartIntent(it))
        }
    }

}