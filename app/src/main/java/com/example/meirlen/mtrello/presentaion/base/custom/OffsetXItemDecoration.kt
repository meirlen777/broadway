package com.example.meirlen.mtrello.presentaion.base.custom

import android.content.Context
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.meirlen.mtrello.R


class OffsetXItemDecoration(context: Context) : RecyclerView.ItemDecoration() {
    private var itemMarginSpan: Int = 0

    init {
        itemMarginSpan = context.resources.getDimension(R.dimen.category_first_item_margin).toInt()
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        super.getItemOffsets(outRect, view, parent, state)
        val position = parent.getChildAdapterPosition(view)
        val isLast = position == state.itemCount - 1
        when {
            isLast -> {
                outRect.right = itemMarginSpan
            }
            position == 0 -> {
                outRect.left = itemMarginSpan
            }
        }
    }

}