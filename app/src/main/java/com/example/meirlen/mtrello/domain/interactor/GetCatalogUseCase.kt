package com.example.meirlen.mtrello.domain.interactor

import com.example.meirlen.mtrello.domain.base.SingleUseCase
import com.example.meirlen.mtrello.domain.repository.CatalogRepository
import com.example.meirlen.mtrello.data.entity.Catalog
import io.reactivex.Single
import javax.inject.Inject

class GetCatalogUseCase @Inject constructor(private val catalogRepository: CatalogRepository) :
    SingleUseCase<List<Catalog>>() {

    override fun buildUseCaseSingle(): Single<List<Catalog>> {
        return catalogRepository.getCatalogTree()
    }

}