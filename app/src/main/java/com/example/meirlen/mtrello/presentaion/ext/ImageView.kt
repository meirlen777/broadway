package com.example.meirlen.mtrello.presentaion.ext

import android.annotation.SuppressLint
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import jp.wasabeef.glide.transformations.RoundedCornersTransformation

internal fun ImageView.loadImage(url: String, width: Int, height: Int) {
    Glide
            .with(this.context)
            .load(url.trim())
            .override(width, height)
            .into(this)
}


fun ImageView.loadWithRoundedCornersImage(url: String, width: Int, height: Int, cornerRadius: Int) {
    Glide
            .with(this.context)
            .load(url.trim())
            .apply(bitmapTransform(MultiTransformation(CenterCrop(),
                    RoundedCornersTransformation(cornerRadius, 0,
                            RoundedCornersTransformation.CornerType.TOP))))
            .override(width, height)

            .into(this)
}


internal fun ImageView.changeColorFilter(@ColorRes id: Int) {
    setColorFilter(ContextCompat.getColor(context, id), android.graphics.PorterDuff.Mode.SRC_IN)
}