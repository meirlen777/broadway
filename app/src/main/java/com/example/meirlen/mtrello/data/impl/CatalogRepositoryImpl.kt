package com.example.meirlen.mtrello.data.impl

import com.example.meirlen.mtrello.domain.repository.CatalogRepository
import com.example.meirlen.mtrello.data.entity.Catalog
import com.example.meirlen.mtrello.presentaion.utill.FakeEnvironment
import io.reactivex.Single

class CatalogRepositoryImpl(private val fakeEnvironment: FakeEnvironment) : CatalogRepository {

    override fun getCatalogTree(): Single<List<Catalog>> {
        return Single.just(fakeEnvironment.getCatalog())
    }
}