package com.example.meirlen.mtrello.presentaion.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.meirlen.mtrello.data.entity.Product
import com.example.meirlen.mtrello.R
import com.example.meirlen.mtrello.presentaion.ext.getDeviceWidth
import com.example.meirlen.mtrello.presentaion.ext.loadWithRoundedCornersImage
import kotlinx.android.synthetic.main.item_product.view.*

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var productList: ArrayList<Product> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val context = holder.view.context
        val product = productList[position]

        holder.itemView.apply {
            productNameTv.text = product.name
            productDescTv.text = product.description
            productPriceTv.text = context.getString(R.string.price_widget_text, product.price.toString())
        }
        setUpProductImageView(holder, context)
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    fun setData(movieList: ArrayList<Product>) {
        productList = movieList
        notifyDataSetChanged()
    }

    fun getProductByPosition(position: Int) = productList[position]

    private fun setUpProductImageView(holder: ProductViewHolder, context: Context) {
        val imageWidth = holder.itemView.getDeviceWidth(context) - (context.resources.getDimension(R.dimen.android_margin_default) * 2)
        val imageHeight = (holder.itemView.getDeviceWidth(context) / 1.7).toInt()
        val cornerRadius = 16
        holder.itemView.productIv.loadWithRoundedCornersImage(context.getString(R.string.fake_product_url), imageWidth.toInt(), imageHeight, cornerRadius)

    }

    inner class ProductViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val view = this.itemView
    }

}